## **iBuy** is a multi-search engine for 3 (amazon.com, aliexpress.com, tap.az) e-commerce websites
### Installation

#### Get the source code  
`git clone https://github.com/odorT/iBuy.git`

## For Windows
### Change environmetn into venv and run the application  
`python3 -m venv env`  
`env/Scripts/activate`  
`pip install -r requirements.txt`  
`python3 application.py`

### After finishing deactivate the venv with:  
`deactivate`

### Notes
The current version only support Chrome browser, you should have already installed Chrome to use this tool
